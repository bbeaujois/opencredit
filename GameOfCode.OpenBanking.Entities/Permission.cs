﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameOfCode.OpenBanking.Entities
{
    public class Permission
    {
        public string BIC { get; set; }
        public string authorization_uri { get; set; }
        public string permissionId { get; set; }
        public string status { get; set; }
    }
}
