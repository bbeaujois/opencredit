﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameOfCode.OpenBanking.Entities
{
    public class Provider
    {
        public string name { get; set; }
        public string BIC { get; set; }
        public string consentModel { get; set; }
    }
}
