﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using GameOfCode.OpenBanking.Business.Interfaces;
using GameOfCode.OpenBanking.Entities;

namespace GameOfCode.OpenBanking.Business.Services
{
    public class LuxHubOneService
    {
        private HttpClient client;

        public AccessToken AccessToken { get; set; }
        private DateTime tokenExpireDate;

        public LuxHubOneService(HttpClient client)
        {
            client.BaseAddress = new Uri("https://apis-sbx.luxhub.com");
            this.client = client;
        }

        public async Task<IEnumerable<Provider>> GetProviders()
        {
            var targeturi = "/oneapi/v1/providers";

            var request = new HttpRequestMessage(HttpMethod.Get, targeturi);
            await GetAccessToken();
            request.Headers.Add("Authorization", $"{AccessToken.token_type} {AccessToken.access_token}");
            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                StreamReader reader = new StreamReader(responseStream);
                var providers = await JsonSerializer.DeserializeAsync<IEnumerable<Provider>>(responseStream);
                return providers;
            }
            else
            {
                throw new Exception(response.StatusCode + ":" + response.ReasonPhrase);
            }
        }

        public async Task GetAccessToken()
        {
            if (AccessToken != null && tokenExpireDate > DateTime.Now)
                return;
            var targeturi = "/api/oauth/token";

            var request = new HttpRequestMessage(HttpMethod.Post, targeturi);
            request.Headers.Add("Authorization", "Basic NTRhMTkwYTgtMjFkOS00MTQyLWFkMjgtNDVlNzM2MDA0OTBlOjZiMzk3ZWMyLTA1MDUtNGZmZi1iYmQ4LWEzYjMxM2M2MTU5NA==");

            HttpContent content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "client_credentials"),
                new KeyValuePair<string, string>("client_id", "54a190a8-21d9-4142-ad28-45e73600490e"),
                new KeyValuePair<string, string>("scope", "OneAPI")
            });

            content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            request.Content = content;

            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                using var responseStream = await response.Content.ReadAsStreamAsync();
                StreamReader reader = new StreamReader(responseStream);
                AccessToken = await JsonSerializer.DeserializeAsync<AccessToken>(responseStream);
                tokenExpireDate = DateTime.Now.AddSeconds(AccessToken.expires_in);
            }
            else
            {
                throw new Exception(response.StatusCode + ":" + response.ReasonPhrase);
            }
        }
    }
}
