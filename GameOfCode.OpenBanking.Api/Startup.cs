using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Resources;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using AutoMapper;
using GameOfCode.OpenBanking.Business.Interfaces;
using GameOfCode.OpenBanking.Business.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Security;

namespace GameOfCode.OpenBanking.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region Automapper configuration
            services.AddAutoMapper(typeof(Startup));
            #endregion
            #region Swagger configuration
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "OpenCreditApi", Version = "v1" });
            });
            #endregion

            #region LuxHub configuration
            services.AddHttpClient<LuxHubOneService>().ConfigurePrimaryHttpMessageHandler(() => {
                var handler = new HttpClientHandler();
                //var cert = new X509Certificate();
                var pemCert = System.IO.File.ReadAllText("Resources/SBX-mTLS-NRAPI-cert.pem");
                var pemKey = System.IO.File.ReadAllText("Resources/SBX-mTLS-NRAPI-key.pem");

                var keyPair = (RsaPrivateCrtKeyParameters)new PemReader(new StringReader(pemKey)).ReadObject();
                var cert = (Org.BouncyCastle.X509.X509Certificate)new PemReader(new StringReader(pemCert)).ReadObject();

                var builder = new Pkcs12StoreBuilder();
                builder.SetUseDerEncoding(true);
                var store = builder.Build();

                var certEntry = new X509CertificateEntry(cert);
                store.SetCertificateEntry("", certEntry);
                store.SetKeyEntry("", new AsymmetricKeyEntry(keyPair), new[] { certEntry });

                byte[] data;
                using (var ms = new MemoryStream())
                {
                    store.Save(ms, Array.Empty<char>(), new SecureRandom());
                    data = ms.ToArray();
                }

                var x509Cert = new X509Certificate2(data);


                handler.ClientCertificates.Add(x509Cert);
                return handler;
            });
            #endregion

            #region Service Configuration
            
            #endregion

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("./swagger/v1/swagger.json", "OpenCredit API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
