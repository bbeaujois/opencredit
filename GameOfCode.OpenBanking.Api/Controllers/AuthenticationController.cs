﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameOfCode.OpenBanking.Business.Services;
using GameOfCode.OpenBanking.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GameOfCode.OpenBanking.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private LuxHubOneService luxHubOneService;

        public AuthenticationController(LuxHubOneService luxHubOneService)
        {
            this.luxHubOneService = luxHubOneService;
        }

        [HttpGet]
        [Route("getToken")]
        public async Task<ActionResult<AccessToken>> GetToken()
        {
            try
            {
                await luxHubOneService.GetAccessToken();
                return Ok(luxHubOneService.AccessToken);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("Providers")]
        public async Task<ActionResult<IEnumerable<Provider>>> Providers()
        {
            //await Task.Run();
            try
            {
                var providers = await luxHubOneService.GetProviders();
                return Ok(providers);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
    }
}
